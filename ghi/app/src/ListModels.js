import React, { useState, useEffect } from 'react';

function ListModels() {
    const [models, setModels] = useState([])
    const deleteModel = async (href) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type": "application/json",
        }
        await fetch(`http://localhost:8100${href}`, fetchConfig)
        window.location.reload()
    }

    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        const modelURL = "http://localhost:8100/api/models/"


        const modelResponse = await fetch(modelURL);
        if (modelResponse.ok) {
            const data = await modelResponse.json()
            setModels(data.models)
        }
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Manufacturer </th>
                        <th> Picture </th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td> {model.name}</td>
                                <td> {model.manufacturer.name}</td>
                                <td>
                                    <img src={model.picture_url} alt="" width="100px" height="100px" />
                                </td>
                                <td>
                                    <button type="button" onClick={() => deleteModel(model.href)} className="btn btn-danger">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>

            </table>
        </div>
    )
}

export default ListModels;