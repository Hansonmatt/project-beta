import React, { useState, useEffect } from 'react';

function SalespeopleList() {
    const [salespeople, setSalesPeople] = useState([])
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        const salespeopleURL = "http://localhost:8090/api/salespeople/"


        const salespeopleResponse = await fetch(salespeopleURL);
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json()
            setSalesPeople(data.salespeople)
        }
    }
    const deleteSalesPerson = async (href) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type": "application/json",
        }
        await fetch(`http://localhost:8090${href}`, fetchConfig)
        window.location.reload()
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th> Employee ID </th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td> {salesperson.first_name}</td>
                                <td> {salesperson.last_name}</td>
                                <td>{salesperson.employee_id}</td>
                                <td>
                                    <button type="button" onClick={() => deleteSalesPerson(salesperson.href)} className="btn btn-danger">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>

            </table>
        </div>
    )
}

export default SalespeopleList;