import { useEffect, useState } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(()=>{
        getData()
    }, [])

    return (
        <div className="container">
            <h1 className="display-5 fw-bold mt-4 mb-4">Technicians</h1>
            <table className="table table-striped table-success">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{tech.employee_id}</td>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default TechnicianList;
