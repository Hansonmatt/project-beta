import React, { useState, useEffect } from 'react';

function CustomerList() {
    const [customers, setSalesPeople] = useState([])
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        const customersURL = "http://localhost:8090/api/customers/"


        const customersResponse = await fetch(customersURL);
        if (customersResponse.ok) {
            const data = await customersResponse.json()
            setSalesPeople(data.customers)
        }
    }
    const deleteCustomer = async (href) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type": "application/json",
        }
        await fetch(`http://localhost:8090${href}`, fetchConfig)
        window.location.reload()
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th> Address </th>
                        <th> Phone Number </th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td> {customer.first_name}</td>
                                <td> {customer.last_name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                                <td>
                                    <button type="button" onClick={() => deleteCustomer(customer.href)} className="btn btn-danger">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>

            </table>
        </div>
    )
}

export default CustomerList;