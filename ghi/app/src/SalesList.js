import React, { useState, useEffect } from 'react';

function SalesList() {
    const [sales, setSales] = useState([])
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        const salesURL = "http://localhost:8090/api/sales/"
        const salesResponse = await fetch(salesURL);
        if (salesResponse.ok) {
            const data = await salesResponse.json()
            setSales(data.sales)
        }
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th> Salesperson Employee ID </th>
                        <th> Salesperson </th>
                        <th> Customer </th>
                        <th> VIN </th>
                        <th> Price </th>
                        <th> Sold </th>
                    </tr>
                </thead>
                <tbody>
                    {
                    sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td> {sale.customer.first_name} {sale.customer.last_name}</td>
                                <td> {sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                                <td>{sale.automobile.sold ? "Yes" : "No"}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesList;